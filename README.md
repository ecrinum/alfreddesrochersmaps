# AlfredDesRochersMaps


Projet de créations de cartes littéraires à propos de l'oeuvre d'Alfred DesRochers

Dans le cadre d'un cours de Stéphanie Bernier, en collaboration avec la [CRCEN](http://ecrituresnumeriques.ca)


FRA3230 – Auteur québécois : Alfred DesRochers, polygraphe
Automne 2022
Vendredi 8h30-11h30

Réalisation d’un itinéraire littéraire

Description de l’activité : réalisation d’un parcours poétique et biographique d’Alfred DesRochers à partir des documents d’archives. Le parcours proposera une relecture de l’œuvre à la lumière des lieux qui la traversent (majoritairement des Cantons de l’Est) et des archives la documentant.

Chaque personne sera responsable d’un lieu du parcours, devra le documenter et en justifier la pertinence et l’intérêt au moment du dévoilement du parcours à la fin du trimestre.

Objectif : prolonger la lecture de l’œuvre au moyen de différents supports et penser sa transmission pour un public non-spécialiste tout en réfléchissant au « sens de l’archive » et aux effets de sa recontextualisation en contexte numérique (Matteo Treleani). 

Lieux de la recherche : Advitam, BAnQ numérique et autres sources.

À déterminer en fonction de la faisabilité :

    • L’intégration de matériel audiovisuel à un lieu (extraits d’émission de télé ou de radio, par exemple).
    • L’ordre de visualisation pourrait être déterminé par des facteurs chronologiques ou textuels (par exemple, tous les lieux reliés au recueil de poésie À l’ombre de l’Orford).
    • La liste des lieux sera déterminée en classe en fonction des recherches et des intérêts des étudiant.e.s.
    • Un atelier et un soutien offerts par un membre de la Chaire en écritures numériques.
    • Le concours de BAnQ Sherbrooke, dépositaire des archives de DesRochers pourrait être envisagée pour une éventuelle diffusion publique. À voir en fonction du résultat.

## Pour faire tourner l'app flask

`python -m venv venv`
ou `python3 -m venv venv`

`source venv/bin/activate`

`pip install flask folium requests pyzotero`

`python app.py`

