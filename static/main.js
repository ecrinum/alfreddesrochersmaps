// global L
const API_URL = '/api/donnees';
let map;

main();

/////////////

async function main() {
  const data = await fetchDonnees();
  console.log({ data })
  // destructurer la réponse
  const {
    coords,
    zoom_start
  } = data;

  // créer la carte sur l’élément avec l’id 'map'
  map = L.map('map').setView(coords, zoom_start);

  // ajouter une couche (ici, celle d’OpenStreetMap par défaut)
  L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
  }).addTo(map);

}

/**
 *
 * @returns {Promise<any>}
 */
async function fetchDonnees() {
  const res = await fetch(API_URL);
  return await res.json();
}
