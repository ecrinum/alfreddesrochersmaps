# Brouillon
<!--Description de l’activité : réalisation d’un parcours poétique et biographique d’Alfred DesRochers à partir des documents d’archives. Le parcours proposera une relecture de l’œuvre à la lumière des lieux qui la traversent (majoritairement des Cantons de l’Est) et des archives la documentant. 

Chaque personne sera responsable d’un lieu du parcours, devra le documenter et en justifier la pertinence et l’intérêt au moment du dévoilement du parcours à la fin du trimestre et dans le cadre d’un court travail écrit.

Objectif : prolonger la *lecture de l’œuvre au moyen de différents supports* et penser sa transmission pour un public non-spécialiste tout en réfléchissant au *« sens de l’archive »* et aux effets de sa recontextualisation *en contexte numérique (Matteo Treleani)*. La cartographie produira une lecture à la jonction d’une poétique du lieu dans une œuvre et de son lieu de production réel  de manière à émettre de nouvelles hypothèses de recherche et interprétations de l’œuvre.--> 

## L'activité 
### Objectif : 
Créer une cartographie des lieux de Alfred DesRochers, pour lire les ouvres du poète à partir de cette nouvelle contextualisation. Comme nous l'a écrit Mme Bernier lors de notre premier échange de courriels, cette perspective spatiale, géographique, vous permettra d'observer les liens qui existent entre l'œuvre littéraire et les lieux, en formulant de nouvelles hypothèses de lecture. Le terme "observation" n'est pas un hasard. Cette lecture spatiale sera d'ailleurs explicitée par une visualisation des données. En saisissant sur Zotero les coordonnées des lieux associés aux documents écrit par DesRochers, de la manière que nous expliquerons plus tard, vous allez en fait remplir une carte. La carte sera affichée sur ce site (pour l'instant, nous avons inséré deux lieux de test) : http://adr.ecrituresnumeriques.ca/   

### Pratiquement 
Vous devrez entrer les données du document de votre choix dans le groupe Zotero [« lieux_deRochers »](https://www.zotero.org/groups/4764734/lieux_desrochers/items/H5C22JAE/library) dans un nouvel élément (démo en direct ?). Dans la section « Marqueurs », vous devrez entrer les coordonnées du lieux associé au document, en respectant exactement cette syntaxe : _Ville : Nom-du-lieu (45.50722944003804, -73.56817483947667).  

Le site web hébergeant le plan du lieu d'Alfred DesRochers récupérera les coordonnées saisies dans les Marqueurs grâce à l'API Zotero.   
En termes très généraux, l'API est une interface de programmation permettant d'utiliser ou de modifier les données d'un programme informatique par le biais de requêtes spécifiques. Dans notre cas, l'API nous permet de récupérer les données de Zotero via un script Python - un langage informatique -, qui produit à son tour le site web.  

### Plan pour la suite de l'intervention
Décomposons maintenant la terminologie que nous avons utilisée jusqu'à présent. Nous allons nous concentrer sur ces points en particulier :
1. Visualisation des données
2. Le logiciel Zotero
3. L'API de Zotero
4. Notre script Python

## 1. Pourquoi la visualisation des données ?
### Voir
La visualisation des données nous permet de mettre en relation les données des nôtres ouvres, des trouver des lien entre eux. Tout d'abord, elle nous sollicite à nous demander ce que l'on veut visualiser. Ce qui signifie  à son tour : qu'est-ce que nous pensons être important, significatif à visualiser ? Cela signifie, après tout, de nous demander : qu'est-ce qui nous semble significatif et important - à découvrir, à savoir, à rechercher, à faire connaître ? 

> Dans notre cas, s'interroger sur la visualisation des données, c'est déjà détailler le champ de notre recherche : c'est définir les sujets et les questions sur lesquels notre recherche va se concentrer. 

En se basant sur la proposition de Mme Bernier, la sélection des œuvres et des lieux, le choix de visualiser les données sur une carte et de les saisir sur Zotero sont les conditions à partir desquelles vous commencerez votre recherche pour l'interprétation des œuvres d'Alfred DesRochers. Ces activités conditionneront donc les résultats de vos travaux finals.
En d'autres mots : En visualisant les liens entre les lieux et les ouvres de Alfred DeRochers, nous sommes déjà en train de penser que les lieux jouent un rôle significatif, important dans notre compréhension des œuvres que nous traitons.

### Ne pas voir
De là, deux autres questions théoriques se posent : 
- Qu'est-ce que notre visualisation ne nous fait pas voir ? 
    1. Notre carte ne nous montre pas l'ensemble de la cartographie de la biographie d'Alfred DesRochers, mais seulement les lieux liés aux œuvres sélectionnées. 
    2. En outre, elle ne nous permet pas d'avoir une vision chronologique des œuvres et des mouvements du poète. 
    3. Nous ne voyons pas non plus combien de temps DesRochers est resté à un certain endroit, et donc dans quelle mesure le lieu a une influence sur l'œuvre.
    4. Nous ne voyons pas le  type de relation qui lie le lieu à l'œuvre : lieu de production, lieu de proclamation, lieu de redaction...
    5. Nous ne voyons pas le type de document concerné (nous imaginons, au contraire, que le rapport de l'auteur au lieu dépend aussi du type de document. Par exemple, l'influence d'un même lieu sera différente si j'écris une lettre à ma famille ou si j'écris un texte pour un examen).

- Quelle est la définition du lieu impliqué dans cette visualisation ?
    1. Les lieux sont ici des espaces de production et de conservation de documents. 
    2. Ces lieux sont les circonstances géographiques, sociales, relationnelles, institutionnelles qui donnent littéralement lieu à l'œuvre
- Et la définition d'œuvre ?
    1. La production de Alfred DesRochers est considérée dans sa matérialité, en tant qu'objet physique, en tant que support, créé et conservé dans un contexte géographique précis. 
    2. L'œuvre est prolongée dans le lieu, ne peut se définir séparément.

## 2. Zotero
### Pourquoi Zotero ?
Zotero « is an open source reference manager tool » (cf. https://www.zotero.org/about/). C'est une interface pour selectionner  les données zotero 

Il nous permet de stocker les données concernant nôtres resources bibliographiques de façon *structurée* et *standardisée*.
Qu'est-ce que cela signifie ? Voyons ces deux dernières termes dans le détail :   
1. Données structurés : les données suivent un modèle, une organisation qui est lisible par nous et par la machine. Les données ont donc une sorte de signification pour la machine aussi (c'est pas le cas si on les écrit dans un fichier texte par example). Ca veut dire que on peut les utiliser dans un code informatique.   
2. Données standardisés : les données sont ici représentés selon un modèle commune, une structure connue par une communauté d'utilisateurs.ices. Notre base de données sera donc inséré dans une logique de niveau supérieur, déjà pensée et partagée. 
Zotero, par example, nous permet de gérer nôtres données bibliographiques de façon standardisée : nous connaissons déjà le type de données que nous pouvons trouver dans un dossier Zotero et la façon dont ce dossier est organisé, avant même d'y accéder. Par contre, on peut très bien utiliser dans notre code un tableau *structuré* en format csv (crée par example avec Excel), mais nous devons d'abord nous renseigner sur la manière spécifique dont il a été conçu et construit.  

Un autre avantage de Zotero c'est que les données sont stockés en ligne et pas seulement dans un fichier en local. Nous pouvons garantir l'accès à notre dossier très facilement à tout le monde....  

Nous sommes en train de donner une représentation numérique de notre resource....  

###  Pourquoi réaliser une carte à partir de Zotero ?  
+ À partir du moment où nous produisons la carte en digital, nous définissons que nôtres ressources bibliographiques, donc littéraires, sont essentiellement liée à l'espace géographique. [à développer]  

+ Nous sommes en train d'afficher une possibile representation de la structure logique de Zotero. [à développer]  

### Est-ce que on aurait eu des solutions alternatives ?
Idéalement, notre structure aurait du représenter les coordonnées aussi de façon structuré. Nous aurons pu utiliser un autre thesaurus (basé sur un standard, comme par example SQL).  

Pour avoir plus des liberté dans la definition des données sans perdre la sémantique des données, on aurait pu utiliser thesaurus qui nous permet une definition de champs en peu plus libre. Mais la structure de Zotero est légère [à développer]  

## L'API zotero
La documentation est accessible ici : https://www.zotero.org/support/dev/web_api/v3/basics.
Il s'agit d'une interface de programmation pour 
 ---

## Python
Introduction: qu'est-ce que Python et pourquoi c'est important de utiliser outils de ce genre en science humaine ? [à développer] 

### Bibliothèques importée
*Sys*
*Folium* : bibliothèque pour la visualisation de données manipules en Python. 
*Requests*
*Json*
*re* : pour les regex

*Pyzotero* : pour utiliser l'API Zotero avec Python (on accède à une porte - qui on appelle endpoint - pour gérer les données du notre dossier Zotero). 

### Récupérer les données
1. Une fois connecté à l'API, on sélectionne tout d'abord notre groupe, via l'identifiant son (4764734), qui on peut trouver aussi dans le lien du groupe : https://www.zotero.org/groups/4764734/lieux_desrochers
2. On doit se positionner au niveau des items, cette à dire au niveau des ressources insérées dans notre groupe.
3. [117] Pour chaque Item, on considère le tags (le marqueurs, ou vous avez inséré les coordonnées des lieux).
4. Pour chaque tag, on sélectionne seulement les coordonnées. Vu que vous avez écrit des autres informations dans les tags (en plus que le coordonnés), on doit utiliser des regex: des requêtes pour sélectionner des parties de texte ; 
    - pour la longitude : on considère se qui vient après la parenthèse d'ouverture et qui vient avant la première virgule 
    - pour la latitude : on considère se qui vient après la virgule et on enlève l'espace entre la virgule et ce qui vient après. Enfin, on enlève aussi la parenthèse de cloture.

### Afficher les données dans une carte
5. On crée une carte - initialement vide - avec 
6. On affiche la longitude et la latitude avec la fonction Marker de la librerie Folium
7. On utilise le format html pour rendre l'affichage plus lisible : on prévoit un titre <h1> (qui correspond au titre de notre resource sur Zotero) ; une Légende (qui affiche ce que vous avez écrit dans l'abstract de la resource) ; une Date (la date de la resource) ; et un Lieu (le lieu - ou place dans la version en anglais - dans Zotero)

N.B. Quand nous insérons les données dans le groupe Zotero, nous devons absolument respecter la syntax que Stéphanie a vous proposée. Parce-que sinon notre truc va pas marcher. Voici l'importance de la standardisation dans un environnement numérique ! [à developer]

### Questions ?