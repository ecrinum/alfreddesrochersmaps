from flask import Flask, render_template, redirect, make_response
import folium # le voici folium - beau nom latin
import requests # il fa falloir faire des demandes, des requêtes, demander de l'information
import json # et l'avoir dans un format, json, qu'il va ensuite falloir lire - mais qui le lit?
import re
import branca
from pyzotero import zotero

app = Flask(__name__, template_folder='./templates/')

zot = zotero.Zotero("4764734", "group")

@app.route('/') # chemine racine
def homepage(): # la fonction qui produit le rendu de cette route
    return render_template('index.html')

# route qui expose les données en json
@app.route('/api/donnees')
def api_donnees():
    # effectuer la requête Zotero
    items = zot.top()
    itemsNotes = []
    for item in items :
        note = zot.children(item['key'])[0]['data']['note']
        # print(note)
        item.update({'note': note})
        itemsNotes.append(item)
    # # définir les données
    itemsNotes = sorted(itemsNotes, key = lambda i: i['data']['date'])
    data = {
        'title': 'Alfred Desrochers',
        'coords': [45.4533, -72.0],
        'zoom_start': 8,
        'items': itemsNotes
    }
    # créer l’objet Response
    resp = make_response(json.dumps(data))
    # nous servons du JSON
    resp.headers['Content-Type'] = 'application/json'
    # CORS: permettre l’accès à partir de n’importe où, ces données ne sont pas privées
    resp.headers['Access-Control-Allow-Origin'] = '*'

    return resp

if __name__ == '__main__':
  app.run(debug=True)
